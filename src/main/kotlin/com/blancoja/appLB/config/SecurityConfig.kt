package com.blancoja.appLB.config

import com.blancoja.appLB.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.util.matcher.AntPathRequestMatcher


@Configuration
@EnableWebSecurity
class SecurityConfig : WebSecurityConfigurerAdapter(){

    @Autowired
    val myUserDetailsService : UserService ?= null


    @Autowired
    private val bcrypt: BCryptPasswordEncoder? = null

    @Bean
    fun passwordEncoder(): BCryptPasswordEncoder? {
        return BCryptPasswordEncoder()
    }

    @Throws(Exception::class)
    override fun configure(auth : AuthenticationManagerBuilder)  {
        auth.userDetailsService(myUserDetailsService).passwordEncoder(bcrypt)

    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity)   {

        http    .csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")//ADMIN role can access /admin/**
                .antMatchers("/shared/**").hasAnyRole("ADMIN","USER")
                .antMatchers("/user/**").hasRole("USER")
                .antMatchers("/**").permitAll()// anyone can access /public/**
                .anyRequest().authenticated()
                .and()
                .formLogin().defaultSuccessUrl("/shared/wellcome", false)
                .and()
                .logout().logoutRequestMatcher( AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/public/goodbye")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .and()
                .httpBasic()
               // .and().formLogin()
               // .defaultSuccessUrl("/shared/hello-world", true)
               // .permitAll()
              //  .and()


    }


}