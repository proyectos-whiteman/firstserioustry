package com.blancoja.appLB.DTOs

import java.util.*
import javax.persistence.Column

class UserDTO (

        val id :String,
        val nombre: String,
        val email: String
        ){
}