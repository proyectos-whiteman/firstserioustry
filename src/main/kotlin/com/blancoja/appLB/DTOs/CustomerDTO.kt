package com.blancoja.appLB.DTOs

import com.blancoja.appLB.model.Address
import com.blancoja.appLB.model.PayMethod

class CustomerDTO (
    val name: String,
    val taxName:String,
    val taxID: String,
    val phone:String,
    val email:String,
    val addresses: Array<AddressDTO>,
    val payMethod: PayMethodDTO
){
}