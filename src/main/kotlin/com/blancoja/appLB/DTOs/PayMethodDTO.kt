package com.blancoja.appLB.DTOs

import com.blancoja.appLB.model.Customer
import javax.persistence.*

class PayMethodDTO (
    val method:String="Transferencia",
    val payDealineInDays:Int=15,
    val payDate: Int=15,
    )
{}