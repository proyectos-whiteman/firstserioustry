package com.blancoja.appLB.DTOs

import com.blancoja.appLB.model.Article

class ArticleDTO (

    val article_ref: String,
    val customer_id: String="generic",
    val prize: Float,
    val description: String,
    val type: String,
    val dimensions: String?=null,
    val color: String?=null,
    val material: String?=null,
){
    companion object{

        fun new(article: Article):ArticleDTO{
            return ArticleDTO(article.article_ref,article.customer_id,article.prize,article.description,article.type,
            article.dimensions, article.color, article.material)
        }
    }
}
