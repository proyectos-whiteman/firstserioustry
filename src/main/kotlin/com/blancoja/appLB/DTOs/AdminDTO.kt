package com.blancoja.appLB.DTOs

import java.util.*
import javax.persistence.Column

class AdminDTO (

        val id :String,
        val nombre: String,
        val email: String,
        val rolPrivilege:String
        ){
}