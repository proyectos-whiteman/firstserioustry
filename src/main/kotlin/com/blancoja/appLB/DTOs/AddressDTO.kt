package com.blancoja.appLB.DTOs
class AddressDTO (
    val adressDetail :String,
    val postalCode : String,
    val city :String,
    val province :String="Barcelona",
    val country :String="Spain",
    val purpose :String ="Invoice"
){
}

