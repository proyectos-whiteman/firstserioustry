package com.blancoja.appLB.model

import com.blancoja.appLB.DTOs.AddressDTO
import com.blancoja.appLB.model.Usuario
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
class ConfirmationToken constructor(user2register:Usuario) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long?=null

    @Column(nullable = true)
    var token: String

    @Column(nullable=true)
    var createdDate: Date? = null

    @Column(nullable = false)
    var user_id: String?=null

    companion object {



    }
    init {
        this.user_id = user2register.id
        createdDate = Date()
        token = UUID.randomUUID().toString()
    }

}