package com.blancoja.appLB.model

import com.blancoja.appLB.DTOs.AddressDTO
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*


@Entity
@Table(name = "address",uniqueConstraints=[UniqueConstraint(columnNames=["customer_id","purpose"])])
class Address (){

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long?=null
    @Column(nullable = false)
    var addressDetail: String?=null
    @Column(nullable = false)
    var postalCode:String?=null
    @Column(nullable = false)
    var city:String="Barcelona"
    @Column(nullable = false)
    var province:String="Barcelona"
    @Column(nullable = false)
    var country:String="Spain"
    @Column(nullable = false)
    var purpose :String="invoice"
    @Column(nullable= false)
    var customer_id: String?=null


    companion object{
        fun create(dto:AddressDTO, customer: Customer):Address{
            val address = Address()
            address.addressDetail=dto.adressDetail
            address.postalCode=dto.postalCode
            address.city=dto.city
            address.province=dto.province
            address.country=dto.country
            address.purpose=dto.purpose
            address.customer_id=customer.id
            return address
        }

    }

    fun equals(adr:Address):Boolean{

        return (this.customer_id==adr.customer_id && this.addressDetail==adr.addressDetail
                && this.city==adr.city && this.country==adr.country && this.postalCode==adr.postalCode
                && this.province==adr.province && this.purpose==adr.purpose)
    }


}
