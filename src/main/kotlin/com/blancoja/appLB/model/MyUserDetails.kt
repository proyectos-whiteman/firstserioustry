package com.blancoja.appLB.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import java.util.stream.Collectors


class MyUserDetails(usuario : Usuario) : UserDetails {
    private val userName: String
    private val password: String
    private val active: Boolean
    private val authorities: List<GrantedAuthority>

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return authorities
    }

    override fun getPassword(): String {
        return password
    }

    override fun getUsername(): String {
        return userName
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return active
    }

    companion object {
        /**
         *
         */
        private const val serialVersionUID = 1L
    }

    init {
        userName = usuario.email
        password = usuario.password
        active = usuario.isEnabled
        authorities = usuario.roles.split(",").filterNot { it.isEmpty() }.map(::SimpleGrantedAuthority)
    }
}