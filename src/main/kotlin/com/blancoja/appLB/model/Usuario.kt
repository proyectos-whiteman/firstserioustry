package com.blancoja.appLB.model




import com.blancoja.appLB.DTOs.UserDTO
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id


@Entity
data class Usuario(

        @Id
        var id: String,
        @Column(nullable = false)
        var userName: String,
        @Column(nullable = false)
        val password: String,
        @Column(name="email",nullable = false, unique=true)
        val email: String,
        @Column(nullable = false)
        val roles :String,
        @Column(nullable = false)
        val isEnabled:Boolean=false
)
{



        companion object {



            fun create(name: String, email: String, password: String): Usuario {
                return Usuario(newId(), name, password, email,"ROLE_USER")
            }

            private fun newId() = UUID.randomUUID().toString()
        }

        fun toDTO(): UserDTO {
              return UserDTO(this.id , this.userName, this.email)
        }


        fun enabled(): Usuario = this.copy(isEnabled = true)



}


