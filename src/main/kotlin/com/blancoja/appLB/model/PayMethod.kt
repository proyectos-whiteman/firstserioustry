package com.blancoja.appLB.model

import com.blancoja.appLB.DTOs.PayMethodDTO
import javax.persistence.*


@Entity
@Table(name="payMethod",
    uniqueConstraints=[UniqueConstraint(columnNames=["method","payDealineInDays","payDate"])])
class PayMethod (){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long?=null
    @Column(nullable = false)
    var method:String="Transferencia"
    @Column(nullable = false)
    var payDealineInDays:Int=15
    @Column(nullable = false)
    var payDate: Int=15


    companion object{
        fun create(dto:PayMethodDTO):PayMethod{
            val method= PayMethod()
            method.method=dto.method
            method.payDealineInDays=dto.payDealineInDays
            method.payDate=dto.payDate
            return method
        }
    }
    fun equals(payMethod: PayMethod):Boolean{
        return this.method==payMethod.method && this.payDate==payMethod.payDate && this.payDealineInDays==payMethod.payDealineInDays

    }

}

