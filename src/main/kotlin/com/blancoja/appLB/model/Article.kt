package com.blancoja.appLB.model

import com.blancoja.appLB.DTOs.ArticleDTO
import com.blancoja.appLB.DTOs.UserDTO
import java.awt.Color
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Article (
    @Id
    val article_ref: String,
    @Column(nullable = false)
    val customer_id: String,
    @Column(nullable = false)
    val prize: Float,
    @Column(nullable = true)
    val color: String?,
    @Column(nullable = true)
    val dimensions: String?,
    @Column(nullable = true)
    val material: String?,
    @Column(nullable=false)
    val type: String,
    @Column(nullable = false)
    val description : String
        ){
    companion object{
        fun create(article_ref: String,customer_id: String="generic",prize: Float,description: String,type: String,
                   dimensions: String?=null, color: String?=null, material: String?=null): Article{
                val articleNew=Article(article_ref,customer_id,prize,color,dimensions,material,type,description)

            return articleNew
        }

        fun create(dto: ArticleDTO): Article{
            val articleNew=Article(dto.article_ref,dto.customer_id,dto.prize,dto.color,dto.dimensions,dto.material,dto.type,dto.description)

            return articleNew
        }
    }

    fun toDTO(): ArticleDTO {
        return ArticleDTO.new(this)
    }

}