package com.blancoja.appLB.model


class TaxID private constructor (val idNumber :String,
                                     var isValidated:Boolean= false,
                                     var type: String="") {





        companion object {
            fun create(idCard: String): TaxID {
                var taxID= TaxID(idCard)
                taxID.type =taxID.type()
                if(taxID.isValid()){
                    taxID=taxID.validated()
                }
                return taxID
            }
        }
        private fun copy(idNumber: String = this.idNumber, validateID : Boolean = this.isValidated, type: String= this.type) = TaxID(idNumber, validateID)


        fun validated(): TaxID = this.copy(validateID = true)

        fun isValid(): Boolean{
            if(this.type=="DNI"||this.type=="NIE"){
                return this.isDNIorNIEValid()
            }else if (this.type=="CIF"){
                return this.isCIFValid()
            }
            return false
        }

        fun isDNIorNIEValid(): Boolean {
            //Si el largo del NIF es diferente a 9, acaba el método.
            var nif = this.idNumber
            if (nif.length != 9) {
                return false
            }
            val secuenciaLetrasNIF = "TRWAGMYFPDXBNJZSQVHLCKE"
            nif = nif.toUpperCase()

            //Posición inicial: 0 (primero en la cadena de texto).
            //Longitud: cadena de texto menos última posición. Así obtenemos solo el número.
            var numeroNIF = nif.substring(0, nif.length - 1)

            //Si es un NIE reemplazamos letra inicial por su valor numérico.
            numeroNIF = numeroNIF.replace("X", "0").replace("Y", "1").replace("Z", "2")

            //Obtenemos la letra con un char que nos servirá también para el índice de las secuenciaLetrasNIF
            val letraNIF = nif[8]
            val i = numeroNIF.toInt() % 23
            return letraNIF == secuenciaLetrasNIF[i]
        }
        fun isCIFValid(): Boolean{
            val cif= this.idNumber
            var pointer=1
            var suma=0
            val secuenciaLetrasCIF = "JABCDEFGHI"
            val entidadLetra="PQRSW"
            val entidadNumero="ABEH"
            for(k in cif.substring(1,8)) {
                val actualNumber=Character.getNumericValue(k)
                if (pointer % 2 == 0) {
                    suma += actualNumber
                } else {
                    suma += if (actualNumber > 4) ((actualNumber * 2) - 9) else (actualNumber * 2)
                }
                pointer += 1
            }
            val decenasSuma=(suma/10).toInt()
            var digitoControl=10-(suma-decenasSuma*10)
            if(digitoControl==10){digitoControl=0}

            if(cif[0].toUpperCase() in entidadLetra || cif.substring(1,3).equals("00")){
                if(secuenciaLetrasCIF[digitoControl].equals(cif[8])){
                    return true
                }
            }else if(cif[0].toUpperCase() in entidadNumero){
                if(Character.getNumericValue(cif[8]).equals(digitoControl)){
                    return true
                }
            } else{
                if(secuenciaLetrasCIF[digitoControl].equals(cif[8]) || Character.getNumericValue(cif[8]).equals(digitoControl)){
                    return true
                }
            }
            return false
        }

        fun type(): String {
            if(this.idNumber[0].isDigit())
                return "DNI"
            if(this.idNumber[0] in 'X'..'Z')
                return "NIE"
            return "CIF"

        }


}

