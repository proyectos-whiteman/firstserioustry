/*
* Copyright 2017 Google, Inc.
* Title: Entity Customer
* Author: JB
* date: 8-March-2021
* Description: entity for DB entries on Table Customer, it has:
*       0. ID
*       1. name(unique)
*       2. invoiceName
*       3. taxId(in spain NIF/NIE/DNI number)
*       4. phoneNumber
*       5. email
*       6. deliveryAddress(street, number,floor, city, postal code)
*       7. invoiceAddress(street, number,floor, city, postal code)(default same direction)
*       8. payMethod(Default transfer without delay)
*
*/

package com.blancoja.appLB.model

import com.blancoja.appLB.DTOs.CustomerDTO
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Email

@Entity
@Table(name="customer")
class Customer(
        @Id
        val id: String,
        @Column(nullable = false, unique=true)
        val customerName: String,
        @Column(nullable = false)
        val taxName: String,
        @Column(nullable = false)
        val taxID: String,
        @Column(nullable = true)
        val phone: String,
        @Column(nullable = true)
        val email: String,
        @Column(nullable=false)
        val payMethod_id: Long

        ){


        companion object{

                fun create(name: String, taxName:String, taxID: String,phone:String,email:String,payMethod: PayMethod): Customer?{
                        val taxId=TaxID.create(taxID)
                        if(taxId.isValidated)
                                return Customer(newId(),name,taxName,taxId.idNumber,phone,email,payMethod.id!!)

                        return null
                }
                fun create(id:String,name: String, taxName:String, taxID: String,phone:String,email:String,payMethod: PayMethod): Customer?{
                        val taxId=TaxID.create(taxID)
                        if(taxId.isValidated)
                                return Customer(id,name,taxName,taxId.idNumber,phone,email,payMethod.id!!)

                        return null
                }
                private fun newId()= UUID.randomUUID().toString()

                //private fun newPayMethod():
        }


}
