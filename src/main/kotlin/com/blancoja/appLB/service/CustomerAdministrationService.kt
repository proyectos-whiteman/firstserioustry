package com.blancoja.appLB.service

import com.blancoja.appLB.DTOs.AddressDTO
import com.blancoja.appLB.DTOs.ArticleDTO
import com.blancoja.appLB.DTOs.CustomerDTO
import com.blancoja.appLB.DTOs.PayMethodDTO
import com.blancoja.appLB.model.Address
import com.blancoja.appLB.model.Article
import com.blancoja.appLB.model.Customer
import com.blancoja.appLB.model.PayMethod
import com.blancoja.appLB.repo.IAddressRepo
import com.blancoja.appLB.repo.IArticleRepo
import com.blancoja.appLB.repo.ICustomerRepo
import com.blancoja.appLB.repo.IPayMethodRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException
import java.util.*
import javax.transaction.Transactional
import javax.xml.crypto.Data
import kotlin.collections.ArrayList


@Service
class CustomerAdministrationService {

    @Autowired
    private lateinit var customerRepo : ICustomerRepo

    @Autowired
    private lateinit var addressRepo : IAddressRepo

    @Autowired
    private lateinit var payMethodRepo: IPayMethodRepo




    fun registerCustomer(dto: CustomerDTO): Customer {

        val payMethod=registerPayMethod(dto.payMethod)

        val customer= Customer.create(dto.name,dto.taxName,dto.taxID, dto.phone, dto.email, payMethod!!)


        try{
            customerRepo.save(customer!!)
        }catch(e:DataIntegrityViolationException){
            println(e)
            throw e
        }catch (e:Exception){
            println(e)
            throw e
        }
        registerAddress(dto.addresses, customer!!)

        return customer
    }

    fun registerAddress(addresses: Array<AddressDTO>, customer:Customer): Boolean{
        for(address in addresses){
            try {
                addressRepo.save(Address.create(address,customer))

            }catch (e:Exception){
                println(e)
                throw e
            }
        }

        return true
    }

    fun registerPayMethod(payMethod: PayMethodDTO): PayMethod? {
        try {
            val payMethodtoSave = PayMethod.create(payMethod)
            val allPAyMethods = payMethodRepo.findAll().filter { pm -> pm.equals(payMethodtoSave) }
            if (allPAyMethods.isEmpty()) {
                return payMethodRepo.save(payMethodtoSave)

            } else {
                return allPAyMethods.firstOrNull()
            }
        }catch(e:IllegalArgumentException){
                println(e)
                throw e
        }catch (e:Exception){
                println(e)
                throw e
            }


        return null

    }

    fun findById(id: String): CustomerDTO? {
        val customer=customerRepo.findByIdOrNull(id)
        val addresses=addressRepo.findAll().filter { address -> address.customer_id==id }
        val payMethod=payMethodRepo.findById(customer!!.payMethod_id).get()
        val payMethoDTO= PayMethodDTO(payMethod.method, payMethod.payDealineInDays, payMethod.payDate)
        val addressesDTO: ArrayList<AddressDTO> = arrayListOf()
        for(address in addresses){
            addressesDTO.add(AddressDTO(address.addressDetail!!,address.postalCode!!,address.city,address.province,address.country,address.purpose))
        }
        if(customer!=null) {
            val customerDTO = CustomerDTO(customer.customerName,customer.taxName,customer.taxID,customer.phone, customer.email,addressesDTO.toTypedArray(),payMethoDTO)
            return customerDTO
        }
        return null
    }

    fun findAll(): List<CustomerDTO>? {
        val customerList= customerRepo.findAll()
        val customerDTOList: ArrayList<CustomerDTO> = arrayListOf()
        for(customer in customerList){
            val customerDTO=findById(customer.id)
            customerDTOList.add(customerDTO!!)
        }

        return customerDTOList
    }

    fun updateCustomer(customerID: String, dto: CustomerDTO): CustomerDTO? {
        val payMethodToSave= registerPayMethod(dto.payMethod)

        val customer= Customer.create(customerID, dto.name,dto.taxName,dto.taxID, dto.phone, dto.email, payMethodToSave!!)


        try {
            customerRepo.save(customer!!)
        }catch(e:IllegalArgumentException){
            println(e)
            throw e
        }catch (e:Exception){
            println(e)
            throw e
        }
//        registerAddress(dto.addresses, customer!!)
//        registerPayMethod(dto.payMethod,customer!!)
        return findById(customerID)
    }

    @Transactional
    fun deleteCustomer(customerID: String):Boolean {
        deleteAddress(customerID)
        try {
            customerRepo.deleteById(customerID)

        }catch(e:IllegalArgumentException){
            println(e)
            throw e
        }catch (e:Exception){
            println(e)
            throw e
        }

        return true
    }

    fun deleteAddress (customerID: String): Boolean {
        val addresses2Die=addressRepo.findAll().filter { address -> address.customer_id==customerID }

        if(addresses2Die.isEmpty()) return false

            try {
                for(address in addresses2Die) {
                    addressRepo.deleteById(address.id!!)
                }
            } catch (e: IllegalArgumentException) {
                println(e)
                throw e
            } catch (e: Exception) {
                println(e)
                throw e
            }

        return true
    }


}