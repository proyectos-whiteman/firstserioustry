package com.blancoja.appLB.service

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.DeleteObjectRequest

import com.blancoja.appLB.DTOs.fileDTO
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.*

import java.lang.Exception
import java.util.*

import javax.annotation.PostConstruct
import java.io.FileOutputStream

import java.io.BufferedOutputStream

import java.io.OutputStream





@Service
class FileService {

    private lateinit var s3client: AmazonS3

    @Value("\${s3.endpointUrl}")
    private val endpointUrl: String? = null

    @Value("\${s3.bucketName}")
    private val bucketName: String? = null

    @Value("\${s3.accessKeyId}")
    private val accessKeyId: String? = null

    @Value("\${s3.secretKey}")
    private val secretKey: String? = null

    @Value("\${s3.region}")
    private val region: String? = null

    @PostConstruct
    private fun initializeAmazon() {
        val credentials: AWSCredentials = BasicAWSCredentials(accessKeyId, secretKey)
        s3client = AmazonS3ClientBuilder
            .standard()
            .withRegion(region)
            .withCredentials(AWSStaticCredentialsProvider(credentials))
            .build()
    }

    @Throws(Exception::class)
    fun uploadFile(dto: fileDTO): String{
        val file: File = File(dto.path+dto.fileName)
                    try {
                s3client.putObject(bucketName, dto.fileName, file)
            } catch (e: AmazonServiceException) {
                return (e.getErrorMessage())

            }
            return "everithing is chachi!!!"


        }

    fun deleteFile(fileName: String): String {
        //val fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1)
        //s3client.deleteObject(bucketName, fileName)

        try {
            println("Deleting file with name= $fileName")
            val deleteObjectRequest = DeleteObjectRequest(bucketName, fileName)
            s3client.deleteObject(deleteObjectRequest)
            println("File deleted successfully.")
        } catch (e: AmazonServiceException) {
            return (e.getErrorMessage())

        }

        return "Successfully deleted"
    }

    fun downloadFile(dto: fileDTO): String {
        try {
            val inputStream  = BufferedInputStream(s3client.getObject(bucketName, dto.fileName).objectContent)
            val file = File(dto.path+dto.fileName)
            val writer: OutputStream = BufferedOutputStream(FileOutputStream(file))
            var read = -1

            while (inputStream.read().also { read = it } != -1) {
                writer.write(read)
            }

            writer.flush()
            writer.close()
            inputStream.close()
        } catch (e: AmazonServiceException) {
            return (e.getErrorMessage())

        }
        return "everithing is chachi!!!"
    }


/*
    @Throws(IOException::class)
    private fun convertMultiPartToFile(file: MultipartFile): File {
        val convFile = File(file.originalFilename)
        val fos = FileOutputStream(convFile)
        fos.write(file.bytes)
        fos.close()
        return convFile
    }
*/

/*    @Throws(Exception::class)
    fun uploadFile2(multipartFile: MultipartFile?): String? {
        var fileUrl = ""
        val file = convertMultiPartToFile(multipartFile!!)
        val fileName = generateFileName(multipartFile)
        fileUrl = "$endpointUrl/$bucketName/$fileName"
        uploadFileTos3bucket(fileName, file)
        file.delete()
        return fileUrl
    }*/

/*
    private fun uploadFileTos3bucket(fileName: String, file: File) {
        s3client.putObject(bucketName, fileName, file)
    }
*/

/*    private fun generateFileName(multiPart: MultipartFile): String {
        return Date().getTime().toString() + "-" + multiPart.originalFilename!!.replace(" ", "_")

    }*/





}