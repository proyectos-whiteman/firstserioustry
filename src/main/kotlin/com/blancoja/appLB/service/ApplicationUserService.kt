package com.blancoja.appLB.service

import com.blancoja.appLB.DTOs.UserCreateDTO
//import com.blancoja.appLB.DTOs.userLoginDTO
import com.blancoja.appLB.model.ConfirmationToken
import com.blancoja.appLB.model.Usuario
import com.blancoja.appLB.repo.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.SimpleMailMessage
import org.springframework.stereotype.Service
import java.util.*


import javax.mail.internet.InternetAddress




@Service
class ApplicationUserService {

    @Autowired
    private lateinit var repo : IUsuarioRepo

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var confirmationTokenRepository: ConfirmationTokenRepository

    @Autowired
    private lateinit var emailSenderService: EmailSenderService

    fun registerUser(userCreateDTO: UserCreateDTO):String{



        //User.create(dto.nombre, ...)
        if (repo.existsByEmail(userCreateDTO.email)) {
            return "user already exists!!"
        } else {
            val usuario = userService.create(userCreateDTO.nombre,  userCreateDTO.password,userCreateDTO.email)

            val confirmationToken = ConfirmationToken(usuario)
            confirmationTokenRepository.save(confirmationToken)
            val mailMessage = SimpleMailMessage()
            mailMessage.setTo(usuario.email)
            mailMessage.setSubject("Complete Registration!")
            mailMessage.setFrom("donotreply@mydomain.com")
            mailMessage.setText("To confirm your account, please click here : "
                    + "http://localhost:8081/confirm-account?token=" + confirmationToken.token)
            emailSenderService.sendEmail(mailMessage)
//            println(confirmationToken.token)
//            val tokenDB = confirmationTokenRepository.findByTokenId(confirmationToken.tokenId!!)
//            println(tokenDB!!.token)
//            val tokenDB2 = confirmationTokenRepository.findByToken(tokenDB.token)
//            println(tokenDB2!!.token)

            return confirmationToken.token.toString()



        }

    }

    fun confirmAccount(confirmationToken: String): Boolean {

        val token = confirmationTokenRepository.findByToken(confirmationToken)
        if (token != null) {

            val user: Usuario =repo.findById(token.user_id!!).get()
            val enabledUser = user.enabled()
            repo.save(enabledUser)
            confirmationTokenRepository.deleteById(token.id!!)

        } else {
            println("this Token is invalid or expired")

            return false
        }
        return true
    } // getters and setters

//    fun login(dto: userLoginDTO): String {
//        return "login successful"
//    }
//
//    fun logout(): String {
//        return "logout successful"
//    }

}
