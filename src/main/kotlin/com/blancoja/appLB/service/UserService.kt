package com.blancoja.appLB.service

import com.blancoja.appLB.DTOs.UserDTO
import com.blancoja.appLB.model.MyUserDetails
import com.blancoja.appLB.model.Usuario
import com.blancoja.appLB.repo.IUsuarioRepo
import net.bytebuddy.implementation.bytecode.Throw
import org.hibernate.exception.ConstraintViolationException
import org.hibernate.mapping.Collection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.ArrayList
import org.springframework.security.core.context.SecurityContextHolder





@Service
 class  UserService : UserDetailsService {

    @Autowired
    private lateinit var repo : IUsuarioRepo

    @Autowired
    val encoder: BCryptPasswordEncoder? = null


    @Throws(FileAlreadyExistsException::class)
    fun create(name: String, password: String, email: String): Usuario {

            val encryptedPassword = encoder!!.encode(password)
            val user = Usuario.create(name,email ,encryptedPassword)
            try{
                return repo.save(user)
            }catch(e:DataIntegrityViolationException){
                println(e)
                throw e
            }catch(e:Exception){
                println(e)
                throw e
            }





    }

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(email: String): UserDetails {

            val usuario : Usuario = repo.findByEmail(email)

            val userDet: UserDetails = MyUserDetails(usuario)

            return userDet

    }


    fun getAll(): List<UserDTO> {
        val users = repo.findAll()  as List<Usuario>
        return users.map {user -> user.toDTO() }
    }

    fun deleteUser(): String {
        val auth: Authentication = SecurityContextHolder.getContext().authentication
        if(repo.existsById(repo.findByEmail(auth.name).id)){
            repo.deleteById(repo.findByEmail(auth.name).id)
            return "Deleted User"
        } else{
            return "user doesn't exists"
        }



    }

    fun getUser(): UserDTO {
        val auth: Authentication = SecurityContextHolder.getContext().authentication




        return repo.findByEmail(auth.name.toString()).toDTO()

    }


}
