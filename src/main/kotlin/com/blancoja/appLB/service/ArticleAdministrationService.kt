package com.blancoja.appLB.service

import com.blancoja.appLB.DTOs.ArticleDTO
import com.blancoja.appLB.DTOs.CustomerDTO
import com.blancoja.appLB.model.Article
import com.blancoja.appLB.repo.IArticleRepo
import com.blancoja.appLB.repo.ICustomerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException


@Service
class ArticleAdministrationService {
    @Autowired
    private lateinit var customerRepo : ICustomerRepo

    @Autowired
    private lateinit var articleRepo: IArticleRepo


    fun registerArticle(dto: ArticleDTO): ArticleDTO? {
        val articleNew= Article.create(dto)
        if(articleNew.customer_id=="generic" || customerRepo.existsById(articleNew.customer_id)){
            try{
                articleRepo.save(articleNew)
            }catch(e: IllegalArgumentException){
                println(e)
                throw e
            }catch (e:Exception){
                println(e)
                throw e
            }
        } else{
            return null
        }


    return articleNew.toDTO()
    }

    fun findById(articleId: String): ArticleDTO? {

        return articleRepo.findById(articleId).get().toDTO()

    }

    fun findAll(): List<ArticleDTO>? {
        val articleList=articleRepo.findAll()
        val articleDTOList: ArrayList<ArticleDTO> = arrayListOf()
        for(article in articleList) {
            articleDTOList.add(article.toDTO())
        }
        return articleDTOList

    }

    fun findAllcustomer(customerId: String): List<ArticleDTO>? {
        val articleList=articleRepo.findAll().filter { article -> article.customer_id==customerId }
        val articleDTOList: ArrayList<ArticleDTO> = arrayListOf()
        for(article in articleList) {
            articleDTOList.add(article.toDTO())
        }
        return articleDTOList
    }



    fun delete(articleRef: String): Boolean {
        try {
            articleRepo.deleteById(articleRef)

        }catch(e:IllegalArgumentException){
            println(e)
            throw e
        }catch (e:Exception){
            println(e)
            throw e
        }

        return true
    }

    fun update(articleRef: String, articleDTO: ArticleDTO): ArticleDTO? {
        articleRepo.deleteById(articleRef)

        try {
            return articleRepo.save(Article.create(articleDTO)).toDTO()
        }catch(e:IllegalArgumentException){
            println(e)
            throw e
        }catch (e:Exception){
            println(e)
            throw e
        }
//        registerAddress(dto.addresses, customer!!)
//        registerPayMethod(dto.payMethod,customer!!)

    }

}
