package com.blancoja.appLB.resources


import com.blancoja.appLB.DTOs.UserCreateDTO
import com.blancoja.appLB.DTOs.UserDTO
import com.blancoja.appLB.model.*

import org.springframework.web.servlet.ModelAndView

import org.springframework.mail.SimpleMailMessage

import com.blancoja.appLB.service.EmailSenderService

import org.springframework.beans.factory.annotation.Autowired

import com.blancoja.appLB.repo.ConfirmationTokenRepository
import com.blancoja.appLB.repo.IUsuarioRepo
import com.blancoja.appLB.service.ApplicationUserService
import com.blancoja.appLB.service.UserService
import org.springframework.http.ResponseEntity

import org.springframework.web.bind.annotation.*


@RestController
class UserAccountController {
//    @Autowired
//    private lateinit var  userRepository: IUsuarioRepo
//
//    @Autowired
//    private lateinit var confirmationTokenRepository: ConfirmationTokenRepository

    @Autowired
    private lateinit var appUserService: ApplicationUserService



    @PostMapping("/register")
    fun registerUser( dto: UserCreateDTO) :ResponseEntity<String> {
        return ResponseEntity.ok(appUserService.registerUser(dto))
    }

    @PostMapping("/confirm-account")
    fun confirmUserAccount(token: String):ResponseEntity<Boolean>
    {
        println(token)
        return ResponseEntity.ok(appUserService.confirmAccount(token))
    }


//    @RequestMapping(value = ["/confirm-account"], method = [RequestMethod.GET, RequestMethod.POST])
//    fun confirmUserAccount(modelAndView: ModelAndView, @RequestParam("token") confirmationToken: String?): ModelAndView {
//        val token = confirmationTokenRepository.findByConfirmationToken(confirmationToken)
//        if (token != null) {
//            val user: Usuario = userRepository.findByEmail(token.user!!.email)
//
//            val enabledUser = user.enabled()
//
//            userRepository.save(enabledUser)
//            modelAndView.viewName = "accountVerified"
//        } else {
//            modelAndView.addObject("message", "The link is invalid or broken!")
//            modelAndView.viewName = "error"
//        }
//        return modelAndView
//    } // getters and setters
}