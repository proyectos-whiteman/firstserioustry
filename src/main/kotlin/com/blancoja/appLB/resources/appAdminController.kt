package com.blancoja.appLB.resources


import com.blancoja.appLB.DTOs.UserDTO
import com.blancoja.appLB.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(value = ["/admin/"])
class appAdminController {

    @Autowired
    private lateinit var userService: UserService

    @PutMapping("/hello-world")
    fun helloWorld(): ResponseEntity<String>? {
        return ResponseEntity.ok("Hello world Admin people")
    }

    @GetMapping("/users")
    fun getUsers(): ResponseEntity<List<UserDTO>> {
        return ResponseEntity.ok(userService.getAll())
    }



}

