package com.blancoja.appLB.resources


import com.blancoja.appLB.DTOs.UserDTO
import com.blancoja.appLB.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/shared/"])
class appSharedController {

    @Autowired
    private lateinit var userService: UserService

    @GetMapping("/wellcome")
    fun helloWorld(): ResponseEntity<String>? {
        return ResponseEntity.ok("Wellcome dear user to AppLB!!!")
    }

    @GetMapping("/users")
    fun getUsers(): ResponseEntity<List<UserDTO>> {
        return ResponseEntity.ok(userService.getAll())
    }



}

