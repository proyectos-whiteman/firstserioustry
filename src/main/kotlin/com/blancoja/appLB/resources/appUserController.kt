package com.blancoja.appLB.resources

import com.blancoja.appLB.DTOs.*
import com.blancoja.appLB.model.Article
import com.blancoja.appLB.model.Customer
import com.blancoja.appLB.model.PayMethod
import com.blancoja.appLB.service.ArticleAdministrationService
import com.blancoja.appLB.service.CustomerAdministrationService
import com.blancoja.appLB.service.FileService
import com.blancoja.appLB.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PathVariable


import org.springframework.data.jpa.domain.AbstractPersistable_.id

import org.springframework.web.bind.annotation.GetMapping
import java.util.*


@RestController
@RequestMapping(value = ["/user"])
class AppUserController {

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var fileService: FileService

    @Autowired
    private lateinit var customerAdminService:CustomerAdministrationService

    @Autowired
    private lateinit var articleAdminService : ArticleAdministrationService

    @GetMapping("/hello-world")
    fun helloWorld(): ResponseEntity<String>? {
        return ResponseEntity.ok("Hello world USERSS")
    }

    @GetMapping("/")
    fun login(): ResponseEntity<String>? {
        return ResponseEntity.ok("Login Hecho!!!")
    }

    @GetMapping("/user")
    fun getUsers(): ResponseEntity<UserDTO> {
        return ResponseEntity.ok(userService.getUser())
    }
    @DeleteMapping("/delete")
    fun deleteUser(): ResponseEntity<String> {
        return ResponseEntity.ok(userService.deleteUser())
    }

    @PostMapping("/fileupload")
    fun fileUpload( dto: fileDTO) :ResponseEntity<String> {
        return ResponseEntity.ok(fileService.uploadFile(dto))
    }

    @PostMapping("/filedownload")
    fun fileDownload( dto: fileDTO) :ResponseEntity<String> {
        return ResponseEntity.ok(fileService.downloadFile(dto))
    }
    @PostMapping("/deletefile")
    fun deleteFile( fileName:String) :ResponseEntity<String> {
        return ResponseEntity.ok(fileService.deleteFile(fileName))
    }

    @PostMapping("/customer-register" ,consumes=["application/json"])
    fun registerCustomer(@RequestBody customerdto: CustomerDTO):ResponseEntity<Customer>{
        return ResponseEntity.ok(customerAdminService.registerCustomer(customerdto))
    }

    @GetMapping("/findcustomer/{customer_id}", consumes = ["application/json"], produces = ["application/json"])
    fun findOneCustomer (@PathVariable customer_id:String): ResponseEntity<Optional<CustomerDTO>> {

        return ResponseEntity.ok(Optional.ofNullable(customerAdminService.findById(customer_id)))

    }

    @GetMapping("/findallcustomer/", consumes = ["application/json"], produces = ["application/json"])
    fun findAllCustomer (): ResponseEntity<Optional<List<CustomerDTO>>> {

        return ResponseEntity.ok(Optional.ofNullable(customerAdminService.findAll()))

    }

    @PutMapping("/updatecustomer/{customer_id}", consumes = ["application/json"], produces = ["application/json"])
    fun updateCustomer (@PathVariable customer_id:String, @RequestBody customerdto: CustomerDTO): ResponseEntity<Optional<CustomerDTO>> {

        return ResponseEntity.ok(Optional.ofNullable(customerAdminService.updateCustomer(customer_id, customerdto)))

    }

    @DeleteMapping("/customer/{customer_id}")
    fun deleteCustomer (@PathVariable customer_id:String): ResponseEntity<Boolean> {

        return ResponseEntity.ok(customerAdminService.deleteCustomer(customer_id ))

    }

    @PostMapping("/article-register" ,consumes=["application/json"])
    fun registerArticle(@RequestBody dto: ArticleDTO):ResponseEntity<Optional<ArticleDTO>>{
        return ResponseEntity.ok(Optional.ofNullable(articleAdminService.registerArticle(dto)))
    }

    @GetMapping("/findarticle/{article_id}", consumes = ["application/json"], produces = ["application/json"])
    fun findOneArticle (@PathVariable article_id:String): ResponseEntity<Optional<ArticleDTO>> {

        return ResponseEntity.ok(Optional.ofNullable(articleAdminService.findById(article_id)))

    }

    @GetMapping("/findallarticle/", consumes = ["application/json"], produces = ["application/json"])
    fun findAllArticle (): ResponseEntity<Optional<List<ArticleDTO>>> {

        return ResponseEntity.ok(Optional.ofNullable(articleAdminService.findAll()))

    }

    @GetMapping("/findarticlecustomer/{customer_id}", consumes = ["application/json"], produces = ["application/json"])
    fun findArticleCustomer (@PathVariable customer_id: String): ResponseEntity<Optional<List<ArticleDTO>>> {

        return ResponseEntity.ok(Optional.ofNullable(articleAdminService.findAllcustomer(customer_id)))

    }

    @PutMapping("/updatearticle/{article_ref}", consumes = ["application/json"], produces = ["application/json"])
    fun updateArticle (@PathVariable article_ref: String, @RequestBody articleDTO: ArticleDTO): ResponseEntity<Optional<ArticleDTO>> {

        return ResponseEntity.ok(Optional.ofNullable(articleAdminService.update(article_ref, articleDTO)))

    }


    @DeleteMapping("/article/{article_ref}")
    fun deleteArticle (@PathVariable article_ref:String): ResponseEntity<Boolean> {

        return ResponseEntity.ok(articleAdminService.delete(article_ref ))

    }


}