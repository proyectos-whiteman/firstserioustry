package com.blancoja.appLB.repo

import com.blancoja.appLB.model.Article
import com.blancoja.appLB.model.Usuario
import org.springframework.data.repository.CrudRepository

interface IArticleRepo : CrudRepository<Article, String> {


}
