package com.blancoja.appLB.repo

import com.blancoja.appLB.model.Customer
import com.blancoja.appLB.model.PayMethod
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository


@Repository
interface IPayMethodRepo : CrudRepository<PayMethod,Long>{


}
