package com.blancoja.appLB.repo

import com.blancoja.appLB.model.ConfirmationToken

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ConfirmationTokenRepository : CrudRepository<ConfirmationToken, Long> {
    fun findByToken(token: String): ConfirmationToken?


}