package com.blancoja.appLB.repo

import com.blancoja.appLB.model.Usuario
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface IUsuarioRepo : CrudRepository<Usuario, String> {


    fun findByEmail(email:String):Usuario

        //fun findByEmailIdIgnoreCase(email: String): Usuario

    fun existsByEmail(email:String):Boolean

}