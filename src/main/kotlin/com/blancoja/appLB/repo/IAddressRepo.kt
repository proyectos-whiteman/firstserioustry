package com.blancoja.appLB.repo

import com.blancoja.appLB.model.Address
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface IAddressRepo : CrudRepository<Address,Long>{

}