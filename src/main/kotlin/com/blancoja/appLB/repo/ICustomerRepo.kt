package com.blancoja.appLB.repo

import com.blancoja.appLB.model.ConfirmationToken
import com.blancoja.appLB.model.Customer
import com.blancoja.appLB.model.Usuario
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ICustomerRepo  : CrudRepository<Customer, String> {

}