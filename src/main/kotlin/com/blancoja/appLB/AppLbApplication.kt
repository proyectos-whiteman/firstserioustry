package com.blancoja.appLB

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@ComponentScan("com.blancoja.appLB")
@SpringBootApplication
class AppLbApplication
//AppService
//  UserService
//          User
//          UserRepository
//  EmailService
//  TokeRepository
// fun createUserAndNotify()



	fun main(args: Array<String>) {
		runApplication<AppLbApplication>(*args)
	}
