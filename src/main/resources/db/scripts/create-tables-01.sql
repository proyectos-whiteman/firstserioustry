
create table pay_method
(
    id bigint not null auto_increment,
    method varchar(20) not null,
    pay_date int not null,
    pay_dealine_in_days int not null,
    PRIMARY KEY (id),
    constraint UK_all_unique unique (method, pay_dealine_in_days, pay_date)
)charset = utf8;

create table usuario
(
    id varchar(100) not null,
    email varchar(200) not null,
    is_enabled bit not null,
    password varchar(255) not null,
    roles varchar(40) not null,
    user_name varchar(40) not null,
    PRIMARY KEY (id),
    constraint UK_email unique(email)
)charset = utf8;

create table customer
(
    id  varchar(100) not null,
    customer_name varchar(40)  not null,
    email varchar(40) null,
    phone varchar(15) null,
    taxid varchar(30) not null,
    tax_name varchar(40) not null,
    pay_method_id bigint null,
    PRIMARY KEY (id),
    constraint UK_customer_name unique(customer_name),
    FOREIGN KEY (pay_method_id) REFERENCES pay_method(id)
)charset = utf8;

create table address
(
    id bigint not null auto_increment,
    address_detail varchar(60) not null,
    city varchar(50)  not null,
    country varchar(15)  not null,
    postal_code varchar(10)  not null,
    province varchar(15)  not null,
    purpose varchar(15)  not null,
    customer_id varchar(100) not null,
    PRIMARY KEY (id),
    FOREIGN KEY (customer_id) REFERENCES customer(id),
    constraint UK_customer_id_purpose
        unique (customer_id, purpose)
)charset = utf8;

create table confirmation_token
(
    id bigint not null auto_increment,
    created_date datetime null,
    token varchar(100) null,
    user_id varchar(100) not null,
    PRIMARY KEY (ID),
    FOREIGN KEY (user_id) REFERENCES usuario(id)
)charset = utf8;




