create table article (
    article_ref varchar(20) not null,
    customer_id varchar(100),
    prize float not null ,
    color varchar(20),
    dimensions varchar(30),
    material varchar(30),
    type varchar(30),
    description varchar(255) not null,

    PRIMARY KEY (article_ref)
)charset = utf8;