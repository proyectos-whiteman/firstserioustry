package com.blancoja.appLB


import com.blancoja.appLB.repo.IUsuarioRepo
import com.blancoja.appLB.service.UserService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder





@SpringBootTest
class AppLbApplicationTests {

	@Autowired
	private lateinit var repo : IUsuarioRepo
	@Autowired
	private lateinit var userService :UserService

/*	@Test
	fun createUserTest() {

		val usu = userService.create("paco234","123345","javiBRu@LB.com")
		//val retorno:Usuario=repo.save(usu)


		assert("javiBRu@LB.com".equals(usu.email) && "paco234".equals(usu.userName) && "123345".equals(usu.password))
	}*/

/*	@Test
	fun toDTOTest(){
		val userReference = userService.create("paco234","123345","javiBRu@LB.com")
		val userCompare=userReference.toDTO()
		assert(userReference.userid.equals(userCompare.id) && userReference.userName.equals(userCompare.nombre) && userReference.email.equals(userCompare.email))
	}*/

	@Test
	fun getAllUsersTest (){
		assert(repo.count()==userService.getAll().size.toLong())
	}

//	@Test
//	fun findByEmailTest (){
//		val userReference = userService.create("paco234","123345","javiBRu@LB.com")
//		assert(repo.findByEmail(userReference.email).email.equals(userReference.email))
//
//	}
}
