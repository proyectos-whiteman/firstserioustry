package com.blancoja.appLB

import com.blancoja.appLB.model.TaxID
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test


class nifAuthenticatortest {

    @Test
    fun DNICheck(){

        val idCard= TaxID.create("41782810K")
        assertTrue(idCard.isDNIorNIEValid())
    }

    @Test
    fun natainvalidDNICheck(){

        val idCard= TaxID.create("46364573R")
        assertFalse(idCard.isDNIorNIEValid())
    }

    @Test
    fun natavalidDNICheck(){

        val idCard= TaxID.create("46364573T")
        assertTrue(idCard.isDNIorNIEValid())
    }
    @Test
    fun validNIECheck(){

        val idCard= TaxID.create("Z8719719X")
        assertTrue(idCard.isDNIorNIEValid())
    }
    @Test
    fun indetifyDNIType(){
        val idCard = TaxID.create("41782810K")

        assert(idCard.type().equals("DNI"))

    }

    @Test
    fun indetifyDNIType2(){
        val idCard = TaxID.create("46364573T")

        assert(idCard.type().equals("DNI"))

    }

    @Test
    fun indetifyDNIType3(){
        val idCard = TaxID.create("39650459T")

        assert(idCard.type().equals("DNI"))

    }
    @Test
    fun indetifyDNIType4(){
        val idCard = TaxID.create("45635255N")

        assert(idCard.type().equals("DNI"))

    }
    @Test
    fun indetifyNIEType1(){
        val idCard = TaxID.create("Z8719719X")

        assert(idCard.type().equals("NIE"))

    }
    @Test
    fun indetifyNIEType2(){
        val idCard = TaxID.create("X7692701Y")

        assert(idCard.type().equals("NIE"))

    }


    @Test
    fun indetifyCIFType(){
        val idCard = TaxID.create("B66712167")

        assert(idCard.type().equals("CIF"))

    }
    @Test
    fun isValidCIFCheck(){

        val idCard= TaxID.create("B66712167")
        assertTrue(idCard.isCIFValid())
    }
    @Test
    fun indetifyCIFType2(){
        val idCard = TaxID.create("D53194403")

        assert(idCard.type().equals("CIF"))

    }
    @Test
    fun isValidCIFCheck2(){

        val idCard= TaxID.create("D53194403")
        assertTrue(idCard.isCIFValid())
    }
    @Test
    fun indetifyCIFType3(){
        val idCard = TaxID.create("U91206326")

        assert(idCard.type().equals("CIF"))

    }
    @Test
    fun isValidCIFCheck3(){

        val idCard= TaxID.create("U91206326")
        assertTrue(idCard.isCIFValid())
    }
    @Test
    fun indetifyCIFType4(){
        val idCard = TaxID.create("H14577134")

        assert(idCard.type().equals("CIF"))

    }
    @Test
    fun isValidCIFCheck4(){

        val idCard= TaxID.create("H14577134")
        assertTrue(idCard.isCIFValid())
    }
    @Test
    fun isValidCIFCheck5(){

        val idCard= TaxID.create("G15774512")
        assertFalse(idCard.isCIFValid())
    }
}
